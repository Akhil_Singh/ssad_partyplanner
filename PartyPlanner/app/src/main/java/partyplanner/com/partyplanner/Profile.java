package partyplanner.com.partyplanner;

import android.util.Log;

/**
 * Created by string on 8/17/2014.
 */
public class Profile {
    int _id;
    String _name;
    String _job;
    String _limit;
    String _city;
    String _phone_number;

    // Empty constructor
    public Profile(){

    }
    // constructor
    public Profile(int id, String name, String job, String city, String _phone_number){
        this._id = id;
        this._name = name;
        this._job = job;
        this._city = city;
        //this._limit = limit;
        this._phone_number = _phone_number;
    }

    // constructor
    public Profile(String name,String job, String city, String _phone_number){
        this._name = name;
        this._job = job;
        this._city = city;
        //this._limit = limit;
        this._phone_number = _phone_number;
        Log.d("Readingasdf: ", "procreated");
    }
    // getting ID
    public int getID(){
        return this._id;
    }

    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting name
    public String getName(){
        return this._name;
    }

    // setting name
    public void setName(String name){
        this._name = name;
    }

    public String getJob(){
        return this._job;
    }

    // setting name
    public void setJob(String job){
        this._job = job;
    }

    public String getCity(){
        return this._city;
    }

    // setting name
    public void setCity(String city){
        this._city = city;
    }
/*
    public String getLimit(){
        return this._limit;
    }

    // setting name
    public void setLimit(String limit){
        this._limit = limit;
    }

*/
    // getting phone number
    public String getPhoneNumber(){
        return this._phone_number;
    }

    // setting phone number
    public void setPhoneNumber(String phone_number){
        this._phone_number = phone_number;
    }

}
