package partyplanner.com.partyplanner;

import android.database.sqlite.SQLiteOpenHelper;
import java.util.ArrayList;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by string on 8/17/2014.
 */
    public class DatabaseHandler extends SQLiteOpenHelper {

        // All Static variables
        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "partyPlanner";

        // Profiles table name
        private static final String TABLE_PROFILES = "profile";

        // Profiles Table Columns names
        private static final String KEY_ID = "id";
        private static final String KEY_NAME = "name";
        private static final String KEY_JOB = "job";
        private static final String KEY_CITY = "city";
        //private static final String KEY_LIMIT = "limit";
        private static final String KEY_PH_NO = "phone_number";

        public DatabaseHandler(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            String CREATE_PROFILES_TABLE = "CREATE TABLE " + TABLE_PROFILES + "(" + KEY_ID + " INTEGER PRIMARY KEY," +
                    KEY_NAME + " TEXT," + KEY_JOB + " TEXT," + KEY_CITY + " TEXT," + KEY_PH_NO + " TEXT " + ")";
            Log.d("Reading: ", "Reading all profilescreate..");
            db.execSQL(CREATE_PROFILES_TABLE);
            Log.d("Reading: ", "Reading all profiles..after");
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_PROFILES);

            // Create tables again
            onCreate(db);
        }

        /**
         * All CRUD(Create, Read, Update, Delete) Operations
         */

        // Adding new profile
        void addProfile(Profile profile) {
            Log.d("Reading: ", "Reading all profiles12..");
            SQLiteDatabase db = this.getWritableDatabase();
            Log.d("Reading: ", "Reading all profiles.234.");

            ContentValues values = new ContentValues();
            Log.d("Reading: ", "Reading all profiles.23423.");
            values.put(KEY_NAME, profile.getName()); // Profile Name
            values.put(KEY_JOB, profile.getJob()); //Job
            values.put(KEY_CITY, profile.getCity()); //city
            //values.put(KEY_LIMIT, profile.getLimit()); //lowest paying amount
            values.put(KEY_PH_NO, profile.getPhoneNumber()); // Profile Phone
            Log.d("Reading: ", "Reading all profiles.3456.");
            // Inserting Row
            db.insert(TABLE_PROFILES, null,  values);
            Log.d("Reading: ", "Reading all profilesennnnnnd..");
            db.close(); // Closing database connection
        }

        // Getting single profile
        Profile getProfile(int id) {
            SQLiteDatabase db = this.getReadableDatabase();

            Cursor cursor;
            cursor = db.query(TABLE_PROFILES, new String[] { KEY_ID,
                            KEY_NAME, KEY_JOB, KEY_CITY, KEY_PH_NO }, KEY_ID + "=?",
                    new String[] { String.valueOf(id) }, null, null, null, null);
            if (cursor != null)
                cursor.moveToFirst();

            Profile profile = new Profile(Integer.parseInt(cursor.getString(0)),
                    cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
            // return profile
            return profile;
        }

        // Getting All Profiles
        public List<Profile> getAllProfiles() {
            List<Profile> profileList = new ArrayList<Profile>();
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_PROFILES;

            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    Profile profile = new Profile();
                    profile.setID(Integer.parseInt(cursor.getString(0)));
                    profile.setName(cursor.getString(1));
                    profile.setJob(cursor.getString(2));
                    profile.setCity(cursor.getString(3));
                    //profile.setLimit(cursor.getString(4));
                    profile.setPhoneNumber(cursor.getString(4));
                    // Adding profile to list
                    profileList.add(profile);
                } while (cursor.moveToNext());
            }

            // return profile list
            return profileList;
        }

        // Updating single profile
        public int updateProfile(Profile profile) {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_NAME, profile.getName());
            values.put(KEY_JOB, profile.getJob());
            values.put(KEY_CITY, profile.getCity());
            //values.put(KEY_LIMIT, profile.getLimit());
            values.put(KEY_PH_NO, profile.getPhoneNumber());

            // updating row
            return db.update(TABLE_PROFILES, values, KEY_ID + " = ?",
                    new String[] { String.valueOf(profile.getID()) });
        }

        // Deleting single profile
        public void deleteProfile(Profile profile) {
            SQLiteDatabase db = this.getWritableDatabase();
            db.delete(TABLE_PROFILES, KEY_ID + " = ?",
                    new String[] { String.valueOf(profile.getID()) });
            db.close();
        }


        // Getting profiles Count
        public int getProfilesCount() {
            String countQuery = "SELECT  * FROM " + TABLE_PROFILES;
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(countQuery, null);
            cursor.close();

            // return count
            return cursor.getCount();
        }

    }