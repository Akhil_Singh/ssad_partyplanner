package partyplanner.com.partyplanner;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;


public class MyPartyActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_party);
        DatabaseHandler db = new DatabaseHandler(this);

        /**
         * CRUD Operations
         * */
        // Inserting Profiles
        Log.d("Insert: ", "Insertinggreay ..");
        db.addProfile(new Profile("Yamaha1", "Guitarist", "Hyderabad", "9100000000"));
        //db.addProfile(new Profile("Yamaha2", "Guitarist", "Hyderabad","50000", "9200000000"));
        //db.addProfile(new Profile("Yamaha3", "Guitarist", "Hyderabad","50000", "9300000000"));
        //db.addProfile(new Profile("Yamaha4", "Guitarist", "Hyderabad","500000", "9400000000"));

        // Reading all profiles
        Log.d("Reading: ", "Reading all profiles..");
        List<Profile> profiles = db.getAllProfiles();

        for (Profile cn : profiles) {
            String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Job: " + cn.getJob();
            // Writing Profiles to log
            Log.d("Name: ", log);
        }

    }
}

















