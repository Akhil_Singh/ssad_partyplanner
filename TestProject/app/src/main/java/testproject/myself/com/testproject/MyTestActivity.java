package testproject.myself.com.testproject;

import java.util.List;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class MyTestActivity extends Activity {

    private Spinner spinner1, spinner2;
    private Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_test);
        DatabaseHandler db = new DatabaseHandler(this);

        /**
         * CRUD Operations
         * */
        // Inserting Profiles
        Log.d("Insert: ", "Inserting ..");
        db.addProfile(new Profile("Yamaha1", "Guitarist", "Hyderabad","5000", "9100000000"));
        db.addProfile(new Profile("Yamaha2", "Guitarist", "Hyderabad","50000", "9200000000"));
        db.addProfile(new Profile("Yamaha3", "Guitarist", "Hyderabad","50000", "9300000000"));
        db.addProfile(new Profile("Yamaha4", "Guitarist", "Hyderabad","500000", "9400000000"));

        // Reading all profiles
        Log.d("Reading: ", "Reading all profiles..");
        List<Profile> profiles = db.getAllProfiles();

        for (Profile cn : profiles) {
            String log = "Id: "+cn.getID()+" ,Name: " + cn.getName() + " ,Phone: " + cn.getPhoneNumber();
            // Writing Profiles to log
            Log.d("Name: ", log);
        }
        addListenerOnButton();
        Button addBtn = (Button)findViewById(R.id.add);
        /*addBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(MyTestActivity.this, addProfile.class);
                startActivity(i);
            }
        });*/


    }


    public void addListenerOnButton() {

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Toast.makeText(MyTestActivity.this,
                        "OnClickListener : " +
                                "\nJob : " + String.valueOf(spinner1.getSelectedItem()) +
                                "\nCity : " + String.valueOf(spinner2.getSelectedItem()),
                        Toast.LENGTH_SHORT).show();
            }





        });
    }
}